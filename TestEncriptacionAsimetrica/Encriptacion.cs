﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace TestEncriptacionAsimetrica
{
    public class Encriptacion
    {
        #region .: Public Methods:.

        public void CreateKeys(out string publicKey, out string privateKey, int keySize = 4096)
        {
            publicKey = null;
            privateKey = null;

            var csp = new CspParameters
            {
                ProviderType = 1,
                Flags = CspProviderFlags.UseArchivableKey,
                KeyNumber = (int)KeyNumber.Exchange
            };

            using var rsa = new RSACryptoServiceProvider(keySize, csp);

            publicKey = rsa.ToXmlString(false);
            privateKey = rsa.ToXmlString(true);
            rsa.PersistKeyInCsp = false;
        }
        public string Decrypt(string input, string privateKey)
        {
            if (input == null)
            {
                throw new Exception("Input cannot be null");
            }
            try
            {
                return Encoding.UTF8.GetString(
               Decrypt(
                   Convert.FromBase64String(input),
                   privateKey));
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public string Encrypt(string input, string publicKey)
        {
            if (input == null)
            {
                throw new Exception("Input cannot be null");
            }

            return Convert.ToBase64String(
                Encrypt(
                    Encoding.UTF8.GetBytes(input),
                    publicKey));
        }

        #endregion .:Public Methods:.

        #region .:Private Methods:.
        private byte[] Encrypt(byte[] bytes, string publicKey)
        {
            var csp = new CspParameters
            {
                ProviderType = 1
            };

            using var rsa = new RSACryptoServiceProvider(csp);

            rsa.FromXmlString(publicKey);
            var data = rsa.Encrypt(bytes, false);

            rsa.PersistKeyInCsp = false;

            return data;
        }
        private byte[] Decrypt(byte[] bytes, string privateKey)
        {
            var csp = new CspParameters
            {
                ProviderType = 1
            };

            using var rsa = new RSACryptoServiceProvider(csp);

            rsa.FromXmlString(privateKey);
            var data = rsa.Decrypt(bytes, false);

            rsa.PersistKeyInCsp = false;

            return data;
        }
        #endregion .:Pivate Methods:.

    }
}
