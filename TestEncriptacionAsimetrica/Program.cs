﻿using System;
using System.IO;

namespace TestEncriptacionAsimetrica
{
    class Program
    {
        static void Main(string[] args)
        {
            string nameInitial = "Sergio Bohorquez";
            Encriptacion encriptacion = new Encriptacion();
            encriptacion.CreateKeys(out string publicKey, out string PrivateKey);
            var encryptName = encriptacion.Encrypt(nameInitial, publicKey);
            //Use an existing path on your computer
            string path = @"C:\Users\SergioStivenBohorque\Desktop\testEncriptacion\nameEncrypt.txt";
            if (!File.Exists(path))
            {
                File.Delete(path);
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.Write(encryptName);
                }
            }
            var decryptName = encriptacion.Decrypt(encryptName, PrivateKey);
            string pathDecrytp = @"C:\Users\SergioStivenBohorque\Desktop\testEncriptacion\nameDecrypt.txt";
            if (!File.Exists(pathDecrytp))
            {
                File.Delete(pathDecrytp);
                using (StreamWriter sw = File.CreateText(pathDecrytp))
                {
                    sw.Write(decryptName);
                }
            }
        }
    }
}
